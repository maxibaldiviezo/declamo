+++
title = 'La voz del que se ha ido'
date = 1
draft = false
+++

##### Autores: Rodolfo Lucca - Demi Carabajal

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/SZWyhuXy8Oo?si=xIgNZ8Iilsy90Z5Q&amp;start=936&amp;end=1046" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/sp5LRa5vl70?si=fYW-iyNAsUTDUzhs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

**Introducción:** $\cases{\text{(G C) x 4 [sin coro]} \\\ \text{4 veces la secuencia (G C) x 4 [con coro]} }$
  

**1)**  

**G**   
Canto del alma  
&nbsp; **C** &nbsp; &nbsp; &nbsp; **G** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**   
voz dolida de pueblo    
&nbsp; **G**   
penas de penas  
**A7** &nbsp; &nbsp; &nbsp; **D/F#**    
piel de los sufrimientos    
&nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**  
remanso del corazón     
&nbsp; &nbsp; **G** &nbsp; &nbsp; &nbsp; **D7** **G** **C**   
vivo recuerdo  

**Interludio:** $\text{ (G C) x 3}$     

**2)**  
Y la distancia  
hace grieta en tu pecho     
para aliviarte  
buscaras un consuelo    
y escucharas el son     
de un canto fresco  

**Interludio:** $\text{ (G C) x 3}$     

**3)**  
Hay un momento      
en que la vida nos gira     
nos lleva lejos     
donde ahora camina     
solo en tu corazón      
quedo encendida     

---
Es la chacarera     
la voz del que se ha ido       
del que se queda    
del abrazo perdido      
grito y luz de la tierra    
en los caminos      

---

**1)**
Desde el olvido     
y esa misma nostalgia       
nace en mis venas   
esa dulce añoranza        
de regresar otra vez        
a tus mañanas       


**2)**
Tal vez la brisa        
me traiga los aromas        
del monte espeso        
de la dulce algarroba       
que siempre los guardare        
en mi memoria       

**3)**
En las bandadas     
se remontan mis sueños      
de acariciarte   
Santiago del estero       
y sentir que estoy vivo     
en tus senderos     

---
Es la chacarera     
la voz del que se ha ido       
del que se queda    
del abrazo perdido      
grito y luz de la tierra    
en los caminos      

---

Sobre el estilo de este aire de chacarera: [Javy Jorge de Metachango](https://www.youtube.com/@javyjorgemetachango2283), uno de los músicos que supieron acompañar de [Demi Carabajal](https://www.youtube.com/@demicarabajaloficial), cuenta como realizar el acompañamiento en guitarra.

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/7pkpFpCb-jY?si=96lUd8dYsYTUUUkf" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

En la siguiente versión se escucha la melodia de la guitarra eléctrica con algunos elementos que la enriquecen.

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/XaJztwu6kio?si=vEy0SzICi7gM-yuh" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---


También está la versión del dúo [Orellana-Lucca](https://www.youtube.com/channel/UCl9d1POUqcj-xi2GKpmRnOQ), cuya versión es estrictamente una chacarera:    


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/WBCmBGXLCew?si=_7UOqTUTA32Xu8JY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/MeqtnF3fYIw?si=Z9zxk6ZTVDuzaK3c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


Existe un tutorial de [Daniel Patanchon](https://www.youtube.com/@danielpatanchon) sobre esta última versión.

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/6bvfTrAZf7o?si=hjd02n-cWz1vcicA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


