+++
title = 'Rumi cani'
date = 1
draft = false
+++

##### Autor: Peteco Carabajal

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/JM5JknLB3FM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/D3cqLFTKPr4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}




**C** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**   
&nbsp; &nbsp; ‘Rumi cani, &nbsp; &nbsp; sapiyqui.   
**C** &nbsp; &nbsp; &nbsp; &nbsp; **Gm/F/Bb**&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**   
&nbsp; &nbsp; Cani huámaj, ckaynayqui.    
**C** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**   
&nbsp; &nbsp; Cani lúchaj, &nbsp; &nbsp; únay pacha,   
**C** &nbsp; **Gm/F/Bb**&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**  &nbsp; **E7**     
&nbsp; &nbsp; amúsaj barromanta.  

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am/G/C**      
Ckonckayqui maa,    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am/G/C**    
huaucke cani.   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;**Am/G/C** &nbsp; &nbsp; &nbsp; **Am/G/F/G/C**     
Razaychista mana huañun.     

(_Modulación de C a D_)

**D** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **D** &nbsp; **D** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **D**   
&nbsp; &nbsp; Piedra soy, &nbsp; &nbsp; soy raíz.    
**D** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am7/G7/C**&nbsp; &nbsp; **D**   
&nbsp; &nbsp; Soy tu origen y tu ayer.    
**D** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **D** &nbsp; &nbsp; &nbsp; **D** &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **D**     
&nbsp; &nbsp; Soy quien lucha &nbsp; &nbsp; con el tiempo   
**D** &nbsp; &nbsp; &nbsp; &nbsp; **Am7/G7/C** &nbsp; **D**   
&nbsp; &nbsp; y del barro volveré.    

**D** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G/A7/D**      
&nbsp; &nbsp; &nbsp; No me olvides,    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G/A7/D**    
soy tu hermano.    
&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;**G/A7/D** &nbsp; &nbsp; &nbsp; **G/A/G/A/D**     
Nuestra raza no se muere.      

**D** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G/A7/D**      
&nbsp; &nbsp; &nbsp; No me olvides, 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G/A7/D**    
soy tu hermano.     
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;**G/A7/D** &nbsp; &nbsp; &nbsp; **G/A/G/A/D**      
Todos juntos llegaremos al Sol.     
  

---
##### Comentarios sobre la letra y el quichua

Las expresiones fonéticas de la letra fueron extraídas de la siguiente [publicación](https://aleroquichua.org.ar/sitio/cancion.php?id=37) de [Alero Quichua Santiagueño](https://www.aleroquichua.org.ar).


En el mismo sitio hay [comentarios](https://www.aleroquichua.org.ar/sitio/destacado.php?id=539) de Crístian Ramón Verduc donde dice:

> _<<Es la voz de un aborigen americano expresándose en quichua. La r inicial en la palabra ‘rumi (piedra) debe pronunciarse simple, como si estuviese escrita dentro de la palabra y no como signo inicial. En esta forma de escribir, la que nos enseñara el Profesor Domingo Antonio Bravo, el apóstrofo hace que la R pase a ser el segundo signo que encontramos en la palabra, sugiriendo así el sonido distinto a cuando es la letra inicial... Peteco Carabajal hace un canto “indio” quichuista en la canción “Rumi Cani: “Ckonckaayqui maa, huaucke cani” (No me olvides, soy tu hermano).>>_     

Una continuidad razonable de este camino es la de aprovechar el [Curso Básico de Quichua](https://www.aleroquichua.org.ar/sitio/curso.php) ofrecido por el mismo sitio.

---
##### Tutorial

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/jvSdG6pyNTM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/9ypQoJsD_vE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


---
##### Otras interpretaciones

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/qTkdUDGGq6M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/es_VV_x8Whw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


---
##### Arreglo coral del Festival Internacional Virtual de Coros Universitarios UNLaM 2021

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/sme983--qD0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

Podes encontrar la partitura del arreglo coral [aquí](/pdf/andes/rumi-cani/rumi-cani.pdf).


