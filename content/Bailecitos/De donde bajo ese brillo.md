+++
title = 'De donde bajo ese brillo'
date = 1
draft = false
+++
##### Autor: Peteco Carabajal

<!--
{{< geogebra  >}}
<div style="text-align: center;">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/rSIrx4v5GWo?si=sqPPQVa7geX1_F1G" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="display: inline-block;"></iframe>
</div>
{{< /geogebra  >}} -->

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/rSIrx4v5GWo?si=sqPPQVa7geX1_F1G" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

**Introducción:** $\cases{\text{B7-E-G#7-C#m} \\\ \text{B7-E-G#7-C#m}}$
  
---
###### Primera Vuelta Instrumental
---
###### Segunda Vuelta
**(I)**  
&nbsp; &nbsp; &nbsp; **C#m**  
De donde bajo ese brillo,   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E**  
del amor como una estrella.     
&nbsp; &nbsp; &nbsp; **F#m**  
Porque nos habrá elegido,   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G#7**  (B7) &nbsp; &nbsp; &nbsp; **C#m**  
para unirnos en quimera.

**(II)**  
De donde partió la flecha,  
que atraviesa corazones.    
Y en un derrumbe de tiempo,     
se hizo fuego en la pasiones    

**(III)**  
&nbsp; &nbsp; **B7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E**    
Puede pasar una vida    
&nbsp; &nbsp; &nbsp; &nbsp; **B7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E**    
sin que a tu pecho lo toquen.   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A**  
No es buena ni mala suerte  
&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **G#7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;**C#m**  
que no pronuncien tu nombre


**(IV)**    
&nbsp; &nbsp; **B7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E**   
Aquí yo estaré esperando    
&nbsp; &nbsp;&nbsp; &nbsp;  **B7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E**     
despierto a la luz del sol.     
&nbsp;  **C#m** &nbsp;  &nbsp;  &nbsp;  **B7** &nbsp;  &nbsp;  &nbsp;  **A**   
Alianza de dos destinos     
&nbsp; &nbsp; &nbsp; **G#m** &nbsp;  &nbsp; **E** &nbsp;  &nbsp;  &nbsp;  &nbsp;  **G#7** &nbsp;  &nbsp;  **C#m**   
que un camino cruzó el amor.     

---
###### Tercera Vuelta: modulación a F#m

**Introducción:** $\cases{\text{E7-A-C#7-F#m} \\\ \text{E7-A-C#7-F#m}}$


**(I)**  
&nbsp; &nbsp; &nbsp; **F#m**  
Compañera, compañero,  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A**  
emparejemos los pasos,  
&nbsp; &nbsp; &nbsp; **Bm**  
los sueños, los pensamientos,   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C#7** (E7) &nbsp; &nbsp; **F#m**  
la memoria del pasado.  

**(II)**  
Si hemos sido señalados    
por el puro movimiento,    
borremos nuestro futuro     
con un mismo sentimiento.    

**(III)**  
&nbsp; &nbsp; **E7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A**    
Puede pasar una vida    
&nbsp; &nbsp; &nbsp; &nbsp; **E7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A**    
sin que a tu pecho lo toquen.   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **D**  
No es buena ni mala suerte  
&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **C#7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;**F#m**  
que no pronuncien tu nombre


**(IV)**    
&nbsp; &nbsp; **E7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A**   
Aquí yo estaré esperando    
&nbsp; &nbsp;&nbsp; &nbsp;  **E7**  &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A**     
despierto a la luz del sol.     
&nbsp;  **F#m** &nbsp;  &nbsp;  &nbsp;  **E7** &nbsp;  &nbsp;  &nbsp;  **D**   
Alianza de los[^1] destinos     
&nbsp; &nbsp; &nbsp; **C#m** &nbsp;  &nbsp; **A** &nbsp;  &nbsp;  &nbsp;  &nbsp;  **C#7** &nbsp;  &nbsp;  **F#m**   
que un camino cruzó el amor.     


[^1]: La versión original de la letra dice <<_dos destinos_>>. Sin embargo, teniendo en cuenta la generalidad de esta tercer vuelta, en la que se le habla a los seres humanos en general, me pareció más consistente que vaya <<_los dentinos_>>.