+++
title = 'Cielo o infierno'
date = 1
draft = false
+++

##### Autor: Demi Carabajal

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/6sqjSyG_m8I?si=_P71VwJC2lY6_LAN" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/l09fDjxGqnc?si=igVb86KeMkOEFyKs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

**Introducción:** $\cases{\text{Bm-F#m-Em-Bm} \\\ \text{Bm-F#m-Em/F#7-Bm}}$
  

**1)**  
**Bm&nbsp; &nbsp; &nbsp; &nbsp; F#m**  
Terriblemente  
**B7&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; Em**  
sacudió su corazón  
**F#7&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;Bm**  
Ojos valientes  
**Em&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; F#7 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bm**  
Pronto pasará el temblor

**2)**  
Allá está el borde  
Quédate, no sigas más  
Después no hay dónde,  
Un sitio para escapar  

**3)**  
**A**  
Aquella niña  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Bm**  
Parecía tan feliz  
**A**  
Mirando al hombre  
**Em** &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **F#7** &nbsp; &nbsp; &nbsp;**Em**  
Prefiere partir de allí 

**Estribillo)**
Quedó en el medio  
A dónde lo llevará  
Cielo o infierno  
Pujando por su lugar  

---

**1)**  
**Bm**&nbsp; &nbsp; &nbsp; &nbsp; **F#m**  
El Supay dentro  
**B7**&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; **Em**  
¡Ordene su majestad!  
**F#7**&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;**Bm**  
Sueño en hogueras,  
**Em**&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **F#7**  &nbsp; &nbsp;**Em**  
Por la bruma en soledad  

**2)**  
Angustia vieja  
Yo no sé si volveré  
Roto los tientos  
A tu salitre morder

**3)**  
**A**  
Dulce y morena  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Bm**  
Conquistó su corazón  
**A**  
Sueña con verla  
**Em** &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **F#7** &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;**Em**  
Para conquistar su amor  

**Estribillo)**
Quedó en el medio  
A dónde lo llevará  
Cielo o infierno  
Pujando por su lugar  

---

##### Tutorial para guitarra

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/i0smugoq5xU?si=OwkdCrBP0ilXG9gB" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

Para visualizar la partitura de Patanchon hace click [aquí](/pdf/chacareras/cielo-o-infierno/cielo-o-infierno-Guitarra.pdf).

##### Tutorial para introducción con piano

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/FOM7_223OMA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

Para visualizar la partitura de Manirano Statello hace click [aquí](/pdf/chacareras/cielo-o-infierno/cielo-o-infierno-Guitarra.pdf).


