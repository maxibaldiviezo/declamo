+++
title = 'Soy de la tierra'
date = 1
draft = false
+++

##### Autor: Horacio Banegas

> Existen dos versiones del mismo autor en dos tonalidades diferentes: en re menor y en mi menor. Originalmente fue hecha en la tonalidad más alta, pero en opciones alternativas el autor la ejecuta en la tonalidad más baja.

---
##### Mi menor

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/qroBNrHCcB4?si=T34Lw0586TLug2t8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/lkJfWVo2n0o?si=cL6diq_xKvOQEZsN" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---
##### Re menor

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/k8ITe1eH6uA?si=4TmRmQVsy-hiHiYf" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}



**1)**  
Soy un latido en la tierra,     
tal vez un pájaro, un alma      
tal vez un pájaro, un alma.     

**2)**      
Soy fantasia constante,     
la voz, el eco y la tarde,      
la voz, el eco y la tarde.      

**3)**      
A donde grillos y estrellas     
se vuelven una ilusión. 

**4)**      
Soy un ayer y un mañana,    
sólo un perfume en el aire,     
pañuelo alegre en la zamba      
terron agreste es mi carne      
Y un triste arrullo de urpila       
agita mi corazón.       

**5)**  
Y en las alas de una nube       
mi corazon alza vuelo    
Y en la flor de una esperanza   
baila un malambo de fuego,      
baila un malambo de fuego.      
Cuando en mis párpados arden    
las rubias trenzas del Sol.       

**6)**      
Hachando el monte del tiempo        
desgajo rima y poesía       
tiernas plegarias que anidan        
en alas algún silencio,     
en alas algún silencio.     
Tierrales del sentimiento     
por donde va mi ilusión.        

**7)**      
Y en las alas de una nube       
mi corazón alza vuelo       
Y en la flor de una esperanza       
baila un malambo de fuego,      
baila un malambo de fuego.      
Cuando en mis párpados arden    
las rubias trenzas del Sol.     

**Final**       
Soy un latido en la tierra,     
tal vez un pájaro, un alma      
tal vez un pájaro, un alma.     

---
##### Tutorial

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/rbecP58K2aY?si=7Ytxc9xUFMvltL5H" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/nNnh6CK9he8?si=dL1bmu4tPwukqazp" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

Para visualizar la partitura de Daniel Patanchon hace click [aquí](/pdf/estilos-santiagueños/soy-de-la-tierra/Soy-de-la-tierra-Dm-Guitarra.pdf). Daniel también se tomó el trabajo de escribirla en la tonalidad de mi menor, mirala [aquí](/pdf/estilos-santiagueños/soy-de-la-tierra/Soy-de-la-tierra-Em-Guitarra.pdf).

---

##### Documental Soy de la Tierra

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/SZWyhuXy8Oo?si=xIgNZ8Iilsy90Z5Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


