+++
title = 'La colina de la vida'
date = 1
draft = false
+++
##### Autor: León Gieco


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/HcVDQxIRxmI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

**Introducción:** $\cases{\text{Bm-B9-Bm-B9-Bm-B9} \\\ \text{E-A-G-D-A(x2)} \\\ \text{E-A-G-D-A(x2)} \\\ \text{G(x2)-Bm(x2)-G-G-Bm-B9} \\\ \text{G(x2)-Bm-B9}  \\\ \text{Bm-B9}}$
  
---
###### Primera Vuelta

**Bm(x4)**  
Casi, casi nada me resulta pasajero,    
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)**    
todo prende de mis sueños   
**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A(x2)-G(x2)**    
y se acopla en mi espalda y así subo,   
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)** &nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp;  **(Bm9 Bm)**     
muy tranquilo la colina de la vida.     


**Bm(x4)**  
Nunca me creo en la cima o en la gloria,    
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)**    
ése es un gran fantasma  
**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E(x2)** &nbsp; &nbsp; &nbsp; &nbsp; **A(x2)-G(x2)**    
creado por generaciones pasadas.    
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)** &nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp;  **(Bm9 Bm)**     
Atascado en el camino de la vida.

$\cases{\text{E-A-G-Bm-B9-Bm} \\\ \text{E-A-G}}$


**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E(x2)**   
La realidad duerme sola en un entierro    
**A(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Bm(x2)**      
y camina triste por el sueño del mas bueno.     
**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A(x2)**    
La realidad baila sola en la mentira        
&nbsp; &nbsp; **F#7(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;**Bm**  &nbsp; **A** &nbsp; &nbsp; **G**    
y en un bolsillo tiene amor y alegría,      
&nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp; &nbsp; **A**  &nbsp; &nbsp; **G** &nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp; &nbsp; **A** &nbsp; &nbsp; &nbsp; **G-Bm**        
un dios de fantasía, la guerra y la poseí-a.           

---

**Introducción:** $\cases{\text{Bm-B9-Bm-B9-Bm-B9} \\\ \text{E-A-G-D-A(x2)} \\\ \text{E-A-G-D-A(x2)} \\\ \text{G(x2)-Bm(x2)-G-G-Bm-B9} \\\ \text{G(x2)-Bm-B9}  \\\ \text{Bm-B9}}$

---
###### Segunda Vuelta
**Bm(x4)**  
Tengo de todo para ver y creer,    
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)**    
por obviar y no creer,   
**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E(x2)** &nbsp; &nbsp; &nbsp; &nbsp; **A(x2)-G(x2)**    
y muchas veces me encuentro solitario   
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)** &nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp;  **(Bm9 Bm)**     
llorando en el umbral de la vida.     


**Bm(x4)**  
Busco hacer pie en el mundo al revés,    
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)**    
busco algún buen amigo  
**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E(x2)** &nbsp; &nbsp; &nbsp; &nbsp; **A(x2)-G(x2)**    
para que me atrape algún día    
**D(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **G(x2)** &nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp;  **(Bm9 Bm)**     
temiendo hallarla muerta a la vida.

$\cases{\text{E-A-G-Bm-B9-Bm} \\\ \text{E-A-G}}$


**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E(x2)**   
La realidad duerme sola en un entierro    
**A(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Bm(x2)**      
y camina triste por el sueño del mas bueno.     
**Bm(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **A(x2)**    
La realidad baila sola en la mentira        
&nbsp; &nbsp; **F#7(x2)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;**Bm**  &nbsp; **A** &nbsp; &nbsp; **G**    
y en un bolsillo tiene amor y alegría,      
&nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp; &nbsp; **A**  &nbsp; &nbsp; **G** &nbsp; &nbsp; &nbsp; **Bm** &nbsp; &nbsp; &nbsp; **A** &nbsp; &nbsp; &nbsp; **G-Bm**        
un dios de fantasía, la guerra y la poseí-a.       

---

Puede ser de ayuda, para entender los rasguidos de la introducción y las estrofas (son diferentes) puede ayudar la siguiente versión:

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/ukKXx-VwaHI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}