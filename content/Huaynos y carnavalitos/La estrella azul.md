+++
title = 'La estrella azul'
date = 1
draft = true
+++

##### Autores: Aitor y María

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/54va6kYuq_s?si=wciP3TSfBJO5V78v" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

**Introducción:** Guitarra con capo en (IV)
  

**1)**  
Cuando viene la luna      
y me lleva  
se lleva mi fragua      
y la noche entera   
Por ella me muero   
igual que ella  
Cuando le canto a la noche a la luna llena  

**Interludio**: saxo    

**2)**  
Y no tengo razón alguna 
mi señora   
que tengo un niño en la cuna    
que se asoma    
ay! que se asoma madre, 
ay! que se asoma    
tengo un niño de lunares    
de lunares su cuna  

**3)**  
Y al alba   
nace un flamenco valiente   
poeta 
de sueño y letras  
que va por la tavernas  
y ahoga sus penas    
lere-lere-lereleee

**4)**  
Serán los mares 
y la luna y sus recuerdos   
serán los días  
ay! su lamento  
y al canto de mi alma 
hay un niño nuevo   
que sabe cuando voy con la luna 
y cuando vuelvo 

**5)**  
Que se cobre    
lo que se debe  
ay con tantas copas de vino  
Que se cobre    
lo que se debe  
ay con tantas copas de vino      
ay mi cuerpo    
no se sostiene    
ay con tantas copas de vino      
ay mi cuerpo    
no se sostiene  

**6)**  
Que me lo tiene que dar 
Que me lo tiene que dar 
ay! el cajón de la boca 
que he para taconea'     
que he para taconea'     
ay! el cajón de la boca     
que me lo tiene que da' 

**7)**
Su madre va con la cuna 
y el niño con la guitarra
buscándole una manera de templarse con el alba  
Su madre va con la cuna 
y el niño con la guitarra
buscándole una manera de templarse con el alba  

**8)**  


##### Tutorial para introducción con piano

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/S8Gd9IzhicY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

Para visualizar la partitura de Manirano Statello hace click [aquí](/pdf/huaynos-y-carnavalitos/la-estrella-azul/la-estrella-azul-introduccion-paino.pdf).
