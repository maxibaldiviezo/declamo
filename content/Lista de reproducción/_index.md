+++
title = 'Lista de reproducción'
date = 2
draft = false
+++

<!--
collapsibleMenu = true # esto agrega el expansor a la entrada del menú si aún no está configurado en tu hugo.toml
[_build]
render = "never" # no se generará una página, por lo que la página no tendrá una URL
-->

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/videoseries?si=isq-zE6fYQgUaBG4&amp;list=PLnUPzA8-x0csycvKbDJgyzNfgqOYVN7V_" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}