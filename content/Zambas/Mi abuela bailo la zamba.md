+++
title = 'Mi abuela bailo la zamba'
date = 1
draft = false
+++

##### Autores: Carlos Carabajal y Peteco Carabajal



{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/TnSr_w26J3s?si=wBIlMlVxEBK9q8zU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

**Introducción:** $\cases{\text{Bm7}^{(b5)}\text{E7-Am-A7-Dm} \\\ \text{E7-Am G7-C E7-Am G F}}$
  

**1)**  

**E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; **Am**   
Nadie bailó la zamba de ayer    
**G7**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **C**   
como lo hacía mi abuela     
**E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **B7** &nbsp; &nbsp; **E7**     
si me parece verla pañuelo al aire volar.       
**E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; **(G)** &nbsp; &nbsp; &nbsp; **C** &nbsp; **(E7)** **(Am-G-F)**     
Si me parece verla en las trinche...ee..ras.

**2)**  
Perfume y agua pa'l carnaval       
corazones ardientes     
las trenzas de las mozas que salen a conquistar.     
**E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **(G)** **C** &nbsp; **(E7)** **Am (G7)**     
Allá por las trincheras su amor encontrarán.     

---
**C** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **(G7)**  &nbsp; &nbsp; **C**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; **(E7)** **Am**   
Repiquetear de bombos llega la gente al lugar.      
**F** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **B7**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **E7**  
Cajones de cerveza de mesa en mesa pa' invitar     
**E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **(G)**  &nbsp; **C** &nbsp; **(E7)** **Am**      
cuando la orquesta suena baila mi abue... ee..la.   

---

**Introducción:** $\cases{\text{Bm7}^{(b5)}\text{E7-Am-A7-Dm} \\\ \text{E7-Am G7-C E7-Am G F}}$

**1)**  
Mariposas al aire se van    
pomos y serpentinas     
sigue la bailarina brindando danza y amor.      
Debajo la enramada la bailarina.        

**2)**  
Años pasados no volverán    
refrescan mi memoria    
jinetes de a caballo en un solo galopar.    
Jinetes de a caballo llegan al carnaval.    

---
Repiquetear de bombos llega la gente al lugar.      
Cajones de cerveza de mesa en mesa pa' invitar      
cuando la orquesta suena baila mi abue... ee..la.   

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/9qFEdlNEh0A?si=yZ1mxrL0bsl369uG" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/HbVIa3ZIL2s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

#### Arreglo para piano

[Aquí](/pdf/zambas/mi-abuela-bailo-la-zamba/mi-abuela-bailo-la-zamba.pdf) hay un arreglo para piano de Mariano Statello, que está vinculado al siguiente video:

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/2a7hlNZ2Xf4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}



