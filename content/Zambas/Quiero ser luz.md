+++
title = 'Quiero ser luz'
date = 1
draft = true
+++

##### Autor: Daniel Reguera

> **Soneto para Reguera**   
<<Si una guitarra triste me dijera    
que no quiere morir entristecida,   
me pondría a rezar sobre su herida  
con tal de recobrar su primavera.   
Si un trovador errante me pidiera   
un poco de luz para su vida,    
toda la selva en fuego convertida   
para su corazón yo le ofreciera.    
Mas, de poco valió la proclamada    
pujanza de mi anhelo, si callada    
la muerte te llevó, Daniel Reguera.     
Pasa tu zamba por la noche oscura,  
y el eco de tu voz en la llanura    
sigue buscando luz y primavera.>>   
Mar del Plata. Febrero de 1965      
Athahualpa Yupanqui


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/IOUIbTjAEOM?si=4YRj5YhFdVtKMlCn" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

**Introducción:** $\text{Dm-Am-E7-Am}$


**1)**  

**Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **(E7)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; **(A7)**  
&nbsp; &nbsp; Se me está haciendo la noche   
**Dm**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Dm**        
En la mitad de la tarde  
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **F**     
&nbsp; &nbsp;  No quiero volverme sombra     
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **B7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **E7**    
Quiero ser luz y quedarme   
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**         
No quiero volverme sombra    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**       
Quiero ser luz y quedarme

**2)**  

**Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **(E7)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; **(A7)**  
&nbsp; &nbsp; Me fui quemando en la noche  
**Dm**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Dm**        
Siguiendo la misma senda 
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **F**     
&nbsp; &nbsp;  Siempre atrás de una guitarra    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **B7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **E7**    
Apagué la última estrella   
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**         
Siempre atrás de una guitarra   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**       
Apagué la última estrella

---

**A7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Dm**      
&nbsp; &nbsp; No sé qué dicha busqué     
**A7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Dm**       
&nbsp; &nbsp;  qué quimera     
&nbsp; &nbsp; &nbsp; &nbsp; **Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **F**   
Qué zamba me quitó el sueño     
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **B7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **E7**    
Qué noche mi primavera      
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**         
Qué zamba me quitó el sueño    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**     
Qué noche mi primavera.     

---

**Introducción:** $\text{Dm-Am-E7-Am}$
**1)**  

**Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **(E7)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; **(A7)**  
&nbsp; &nbsp; Hoy que me pongo a pensar   
**Dm**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Dm**        
Solo converso en silencio  
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **F**     
&nbsp; &nbsp;  Me miran los ojos de antes     
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **B7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **E7**    
Viejos de ausencia y de tiempo   
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**         
Me miran los ojos de antes    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**       
Viejos de ausencia y de tiempo

**2)**  
**Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **(E7)** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; **(A7)**  
&nbsp; &nbsp; La misma mirada siempre  
**Dm**  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Dm**        
De aquellos ojos tan lejos 
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **F**     
&nbsp; &nbsp;  Por fin me duermo en la noche    
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **B7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; **E7**    
Que alumbra el lucero viejo   
**Dm** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**         
Por fin me duermo en la noche   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; **E7** &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **Am**       
Que alumbra el lucero viejo   

---
No sé qué dicha busqué      
Ay, qué quimera     
Qué zamba me quitó el sueño     
Qué noche mi primavera      
Qué zamba me quitó el sueño     
Qué noche mi primavera      

---

#### Artículo relacionado

[Aquí](https://revistafolklore.com.ar/daniel-reguera/) hay un artículo muy interesante para leer.

#### Introducción un poco más elaborada

En el siguiente video hay una introducción sencilla para aprender.

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/V9FBVXYdgG8?si=Hth3FvS5SkH1D_wI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

#### Otras interpretaciones

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/UG3wPx8zKbk?si=tglLE6wnAMoC7twV" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/qzZR3KS4gfU?si=Z-duMXGUqFe_Gu48" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/f0oxt2ekr9U?si=bzA-76pO_Lg5-90e" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/lDFzzd4OkBA?si=KZSlt1z2UnRejV2E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/CJLzOr7uSIU?si=Fd_3HtjYxZNrRgjX" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}