+++
title = 'Zamba para la viuda'
date = 1
draft = false
+++

##### Autor: Gustavo "El Cuchi" Leguizamón




{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/S80Fn4c4HAA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/S80Fn4c4HAA&amp;start=3&amp;end=1046" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/N4LkBbyZbNI?si=QxrbjLaaWza0LbsZ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}


  

**1)**  
Entre las sendas del monte,      
trapito de nube oscura,      
desflecándose en el aire    
va la sombra de la viuda.    
La dibuja el refucilo.   
le moja el pelo la lluvia.   

**2)**  
Por su domador se apena. 
El de la voz corajuda,   
el que habiloso llenaba     
de escozores su cintura.     
Pena por el rubio Soria,     
difunto y sin sepultura     

**Estribillo)**  
Noche de las condenadas,     
noche parda de las brujas.       
Desflecándose en el aire,        
trapito de nube oscura:          
anda en el monte llorando       
lágrimas de ánima y viuda.       

---

**1)**  
Cuando la oración enciende      
los ojos a las lechuzas     
y el Horco Molle se apaga       
al fondo de la laguna:       
los gauchos ven en las sombras      
al fantasma de la viuda.     

**2)**  
Tiemblan caballo y jinete       
cuando se enanca la viuda.       
Es pedigüeña de amores      
y si el hombre se le asusta:     
le clava en medio del pecho,     
mismo que garras, las uñas.   

**Estribillo)**  
Noche de las condenadas,     
noche parda de las brujas.       
Desflecándose en el aire,        
trapito de nube oscura:          
anda en el monte llorando       
lágrimas de ánima y viuda        

---

##### Partitura para piano

[Aquí](/pdf/zambas/zamba-para-la-viuda/Zamba-para-la-viuda-Piano.pdf) podes encontrar una partitura para piano en la tonalidad de $Ab$.

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/tXzfk_pluIw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/9C4eY3nUZu0?si=0XZYRnTUp3T4iGLA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

--- 

##### Partitura para guitarra

La siguiente transcripción para guitarra está en la tonalidad de $E$.

{{< figure src="/pdf/zambas/zamba-para-la-viuda/Zamba para la viuda 1.jpg" title="" caption="" id="" attr="">}}
{{< figure src="/pdf/zambas/zamba-para-la-viuda/Zamba para la viuda 2.jpg" title="" caption="" id="" attr="">}}
{{< figure src="/pdf/zambas/zamba-para-la-viuda/Zamba para la viuda 3.jpg" title="" caption="" id="" attr="">}}
{{< figure src="/pdf/zambas/zamba-para-la-viuda/Zamba para la viuda - Armonía 1.jpg" title="" caption="" id="" attr="">}}
{{< figure src="/pdf/zambas/zamba-para-la-viuda/Zamba para la viuda - Armonía 2.jpg" title="" caption="" id="" attr="">}}
{{< figure src="/pdf/zambas/zamba-para-la-viuda/Zamba para la viuda - Letra.jpg" title="" caption="" id="" attr="">}}

--- 

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/nAXEttSV91g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

#### Tutorial de acompañamiento


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/tda0CnRD-o8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

[Aquí](/pdf/zambas/zamba-para-la-viuda/Zamba-para-la-viuda-Acompañamiento.pdf) aquí está la transcripción del tutorial en la tonalidad de $Ab$.

---

##### Interpretaciones varias

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/swd3LyJeSx0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/TbMNK_-aXp0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}

---

{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/xrTQdNmaEos" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}



