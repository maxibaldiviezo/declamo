+++
archetype = "home"
title = "Declamo"
+++

## Bienvenido/a

> _<< En la declamación del canto. Si se quiere, esa tragedia, esa alegría, que le da la declamación. **Porque esas letras hay que aprenderlas y declamarlas primero. Y después hacerlas canto**. Eso es lo que yo pretendo, que lo hagan ellos, para el bien de ellos. ¿Qué voy a cosechar yo de eso? Nada más que la alegría de haber dado una mano a alguien que lo ha hecho, con la más pureza, con el más puro sentimiento que podía tener.>>_ -  **Rolando "El chivo" Valladares**

{{< geogebra  >}}
<div style="text-align: center;">
    <audio controls style="margin: 0 auto;">
        <source src="/sound/home/declamar.mp3" type="audio/mpeg">
        Tu navegador no soporta la reproducción de audio.
    </audio>
</div>
{{< /geogebra  >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/ps9AIxpO0KU?si=H_K2PdOjTapZ_meI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/ps9AIxpO0KU?si=H_K2PdOjTapZ_meI&amp;start=428&amp;end=449" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}



{{< geogebra  >}}
<div style="overflow:hidden; padding-bottom:56.25%; position:relative; height:0;">
    <iframe style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://www.youtube.com/embed/kjmTSJhG4ss?si=_v1qmHUJtndSzCXB&amp;start=225&amp;end=270" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
{{< /geogebra  >}}



